---
eleventyNavigation:
  key: CloneCommitViaWeb
  title: Clone & Commit via Web
  parent: Git
  order: 30
---

Clone, edit, commit, push and pull can be performed using Git directly from the command line, by using a Git client, or via the web interface. The former option is shown in the sections [Clone & Commit via CLI](/git/clone-commit-via-cli). The latter option is detailed below.

The user in these examples is `knut` the polar bear and its repository is `foobar`. The repository was created via the Codeberg website including a `README.md` file.

## Clone
*Cloning* refers to the process of creating an identical copy of a repository to the local machine.

Copy the repo URL from the Codeberg website to your Git client using the `Copy` icon.

> If you want to download a copy of a specific state of the repository, without its version history, click on the `Download Repository` icon to download either as ZIP or TAR.GZ.

<picture>
  <source srcset="/assets/images/git/clone-commit-via-web/clone.webp" type="image/webp">
  <img src="/assets/images/git/clone-commit-via-web/clone.png" alt="clone">
</picture>

## Edit
Click on the file you want to edit from the list of files in the repo. Let's try it here with the `README.md` file.
The pencil tool (`Edit File`) will open a new window.

<picture>
  <source srcset="/assets/images/git/clone-commit-via-web/edit1.webp" type="image/webp">
  <img src="/assets/images/git/clone-commit-via-web/edit1.png" alt="edit1">
</picture>

There you can edit the file as you wish.
The `Preview` tab shows you how the file will look like, and the `Preview Changes` will highlight the changes to the file (red for deletions and green for additions).

<picture>
  <source srcset="/assets/images/git/clone-commit-via-web/edit2.webp" type="image/webp">
  <img src="/assets/images/git/clone-commit-via-web/edit2.png" alt="edit2">
</picture>

## Commit
A *commit* is a record of the changes to the repository. This is like a snapshot of your edits.

The commit section is at the bottom of the edit window:

<picture>
  <source srcset="/assets/images/git/clone-commit-via-web/commit.webp" type="image/webp">
  <img src="/assets/images/git/clone-commit-via-web/commit.png" alt="commit">
</picture>

A commit requires a commit message. A default message is added, but do not hesitate to edit it. Make sure your commit message is informative, for you, your collaborators and anyone who might be interested in your work. Some advice on how to write a good commit message can be found on countless websites and blogs!

If you intend to start a pull request with this commit, you should choose the option `Create a new branch for this commit and start a pull request`. It will make it easier to work on the different commits without mixing them if they are in different forks. Check the documentation on [Pull requests and Git flow](/collaborating/pull-requests-and-git-flow) for more details.

Submit your changes by clicking on `Commit Changes`.

## Push and pull
Synchronizing the modifications (commit) from the local repository to the remote one on Codeberg is called *pushing*.

*Pulling* synchronizes the modifications (commit) from the remote repository on Codeberg to the local one.
Pulling is important when you work on different computers to make sure that all computers are on the same stage. It is even more important when you have collaborators on a project; they might change the files too, so you need to pull these modifications before you start working.
Because of that, it is recommended to pull before pushing.

Pushing and pulling make sense only if you work locally. This is why there is no "push" or "pull" button on the Codeberg web interface; committing there already pushes to the remote repository on Codeberg, and there is therefore nothing to pull (except pull requests of course).
