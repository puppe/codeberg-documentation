---
eleventyNavigation:
  key: StyleGuide
  title: Style Guide
  parent: ImprovingCodeberg
---

The present style guide aims at streamlining the authoring process. We would like to maintain a consistent style to guarantee a pleasant reading experience. If you haven't done it already, please read the [Documentation Contributor FAQ](https://docs.codeberg.org/improving-codeberg/docs-contributor-faq/) before using this guide.

A style guide is meant to be used as a reference when there is a doubt about a style convention. Think of it as a set of recommendations to avoid time-consuming discussions about grammar, typography, and layout aspects.

It is not a novel that we expect you to read from A to Z. For instance, if you want to know how to format date and numbers, you can head immediately to the relevant section under [Dates, times, and numbers](#dates%2C-times%2C-and-numbers).

In an ideal world, the reviewer is the person that must be in charge of enforcing the guidelines provided in the style guide.

> **Note:**
> This style guide is a starting point. Feel free to create an issue, open a Pull Request, and modify it according to the community's needs.

We intend to reach people from different knowledge backgrounds and life experiences. Therefore, we kindly ask you to follow these rules:

| Rule                       | What we recommend                                                                                              |
|:---------------------------|:---------------------------------------------------------------------------------------------------------------|
| Be as neutral as possible  | Avoid using slang and colloquial expressions                                                                   |
| Be as clear as possible    | Avoid wordy sentences. Don't expect your readers to “guess” the intended meaning from context                  |
| Be as concise as possible  | Keep your sentences short and prefer active over passive voice                                                 |

## Organization of the style guide

We are still at the beginning of our documentation journey. As of now, the style guide consists of three main components:

1. [Layout and typography](#layout-and-typography)
2. [Document structure](#document-structure)
3. [Wording and terminology](#wording-and-terminology)

Feel free to suggest any additional points and/or sections you deem necessary.

## Layout and typography

Follow these guidelines to organize the structure of your document:

### Headings 

- Keep your headings short.
- Do not use periods (.) and colons (:) at the end of your headings.
- Avoid using consecutive headings without any content in between.
- Avoid using abbreviations in the headings. This particularly applies to lesser known abbreviations.
- Use sentence-style capitalization, i.e. capitalize only the first letter of the first word in the heading as well as words where capitalization is compulsory, such as proper nouns. 
- Your headings must reflect the content of the text that follows. 
- Begin your headings with the following parts of speech according to the typology of your content:

| Content type | What we recommend   | Explanation                                                                                             |
|:-------------|:--------------------|:--------------------------------------------------------------------------------------------------------|
| Conceptual   | Noun or noun phrase | A conceptual text explains a concept, e.g. “Advantages of version control”                              |
| Procedural   | Gerund              | A procedural text describes a procedure or task, e.g. “Installing Git on you local machine”             |
| Reference    | Noun or noun phrase | A reference text provides information about different elements of a set, e.g. “Keyboard shortcuts” and “File formats”|

### Unordered and ordered lists

By unordered lists, we mean **bullet-style** lists. Use unordered lists when the sequence is irrelevant, e.g.:

- The print() function
- The input() function
- The len() function

By ordered lists, we mean **numbered** lists. Use ordered lists to describe procedures that involve multiple sequential steps, e.g.:

1. Type the URL into your browser's address bar
2. Press the Enter button
3. Go to the Login section and click on “Forgot Password”

Maintain a consistent punctuation style in lists. Decide whether you will use “complete sentences” or “sentence fragments” in your list items. Add a period (.) at the end of each list item if you choose to use complete sentences. Do not add a period at the end of each item if you are only using sentence fragments.

### Straight and curly quotes

Use curly quotes instead of straight quotes, e.g.:

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| "That's an 'interesting' task"      | “That's an ‘interesting’ task”                                                                     |
| "We're 'happy' that you're here"    | “We're ‘happy’ that you're here”                                                                   |

### Dates, times, and numbers

Please follow the conventions below to ensure a consistent format of dates, times, and numbers.
  
#### Dates and times

Use the following format for dates in running text: **dd MMMM yyyy**, e.g. 9 March 2022.

Use the following “short” format for dates outside the running text: **dd.mm.yyyy**, e.g. 9.3.2022.   

Do not use leading zeros in dates, e.g.:

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| 09 March 2022                       | 9 March 2022                                                                                       | 
| 09.03.2022                          | 9.3.2022                                                                                           |

Use the 24-hour system. Do not use the 12-hour system with a.m. and p.m.

Do not use leading zeros to indicate time. Write full hours with zero minutes.

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| 6.20 p.m                            | 18.20                                                                                              | 
| 12                                  | 12.00                                                                                              |
| 09.03.2022                          | 9.3.2022                                                                                           |

#### Numbers

Write out the numbers zero to nine in letters. 

Use digits for all numbers above 9. 

Use a combination of digits and letters when you have numbers that involve millions, billions, etc.

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| 0 to 9                              | zero to nine                                                                                       | 
| Twenty five pages                   | 25 pages                                                                                           |
| Twenty billion                      | 20 billion                                                                                         |


### Hyphenation

Write “**email**” in lowercase characters without a hyphen. Do not use any other variant such as “e-mail” or “E-mail”, etc.

Use hyphens with compound adjectives and adverbs before nouns to eliminate any possible ambiguities, e.g.:

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| Self evaluating object              | Self-evaluating object                                                                             | 
| Well formed XML documents           | Well-formed XML documents                                                                          |
| Low level programming language      | Low-level programming language                                                                     |
| High performance databases          | High-performance databases                                                                         |
| Impossible to type characters       | Impossible-to-type characters                                                                      |

Do not use hyphens for adjectives and adverbs ending in **-ly**, e.g.

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| Fully-qualified domain name         | Fully qualified domain name                                                                        | 
| Highly-scalable application         | Highly scalable application                                                                        |
| Dynamically-typed languages         | Dynamically typed languages                                                                        |

## Document structure

If you write an excessively long article, your readers might get lost on the way. Break your article down in smaller articles for a better reading experience.

Avoid the excessive use of hyperlinks and cross-references within your document to prevent readers from being distracted. 

### Sentences and paragraphs

A standard paragraph consists of three to eight sentences. If a paragraph exceeds the maximum allowed number of sentences, reorganize the information to enhance readability.

Favor “self-contained information units” over interconnected information units. An information unit is called self-contained when it does not depend on other information units to convey meaning. Interconnected information units make an excessive use of pronouns and conjunctions such as “this”, “it”, “consequently”, “therefore”, etc.

### Notes and warnings

Keep notes and warnings outside the body of your running text. Use appropriate Markdown syntax to display notes and warnings, e.g. using the following markdown syntax:

```markdown
> **Note:**
> Always write the names of measurement units in lowercase letters, i.e. use “watt” instead of “Watt”.
```

which will be rendered as:

> **Note:**
> Always write the names of measurement units in lowercase letters, i.e. use “watt” instead of “Watt”.

### Topic typology

Adopt one of the following approaches according to the typology of your content (cf. the difference between “conceptual”, “procedural”, and “reference” content under the section [Headings](#headings)):

| Content type | Recommended structure                                                                                                         |
|:-------------|:------------------------------------------------------------------------------------------------------------------------------|
| Conceptual   | Use Definitions to introduce the reader to the subject matter before adding your explanations. Use paragraphs and unordered lists to explain the concepts.|
| Procedural   | Provide a brief introduction to the procedure. Point out any prerequisites before you start describing the steps to follow. Use ordered lists for the different tasks|
| Reference    | Provide a brief introduction in each section to help your readers decide whether they need this particular topic. Use subsections, tables and bullet-style lists to ensure easy access to information.|

## Wording and terminology

- Use simple and easy-to-understand terms in your writing
- Avoid references to culture-specific examples that people from other countries or continents may not understand
- Do not coin terms, i.e. create new ones, if a term denoting the same concept already exists
- Explain terms if you think that some readers may either not understand them or misinterpret them  
- Do not use double negatives, e.g.:

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| You're not going nowhere       | You're going nowhere                                                                                    | 
| He didn't see nothing          | He didn't see anything                                                                                  |

We use the standard American spelling conventions, i.e.:

| Do not write                        | Write                                                                                              |
|:------------------------------------|:---------------------------------------------------------------------------------------------------|
| flavour; colour; labour             | flavor; color; labor                                                                               | 
| analyse; organise; standardise      | analyze; organize; standardize                                                                     |
| catalogue; analogue, dialogue       | catalog, analog, dialog                                                                            |
| defence, licence                    | defense, license                                                                                   | 

- Start names with an upper case letter (e.g. Git instead of git, Markdown instead of markdown) 