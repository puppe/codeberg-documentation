---
eleventyNavigation:
  key: MarkdownFAQ
  title: Markdown FAQ
  parent: Markdown
  order: 90
---

This section contains frequently asked questions regarding the use of Markdown on Codeberg.

## Why doesn't my markdown render correctly?

Sometimes markdown is rendered differently on different sides. If your markdown renders
correctly on another forge or in your editor but does not get rendered correctly at Codeberg,
it is probably due to a difference in the Markdown flavour used by the side/editor.

Codeberg uses Gitea at it's core. Gitea uses [Goldmark](https://github.com/yuin/goldmark) as rendering engine.
Goldmark is compliant with [CommonMark 0.30](https://spec.commonmark.org/0.30/).

Please refer to the CommonMark 0.30 specification on what gets rendered and why.

If you want to correct your rendering you either have to adapt to Codebergs rendering or
you must find another way to find an approach common to platforms/software used (and/or supported) by you.