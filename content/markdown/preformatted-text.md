---
eleventyNavigation:
  key: PreformattedText
  title: Preformatted Text
  parent: Markdown
  order: 50
---

There are two ways to use preformatted text withing your Markdown document:

- By using indentation
- By using one or more backticks at the beginning and the end of a preformatted section

## Using indentation

You can preformat a section of text or code by indenting the code with 4 or more spaces or a tab:

    this
    is
    displayed
    as
    preformatted

It is not possible to add a rendering hint. It is not possible to preformat text within a line using this syntax.

## Using backticks

You can preformat a section of text by starting a section of text with one or more backtick characters.

```
this
is
displayed
as
preformatted
```

You can also preformat a section of text within a line using the backtick syntax.
The following text is for example `preformatted` by using the backtick syntax.

### Rendering hints

Sometime renders use hints to syntax highlight the code in a preformatted section.

To provide a hint, simple add the language name at the end of the introductory backtick(s).

For example using `shell` as hint will tell the renderer that the given code can be highlighted as shell script:

```shell
#!/bin/bash

echo "Hello world"
```

The same would be rendered without syntax highlighting if the hint is not given:

```
#!/bin/bash

echo "Hello world"
```
